#from flask import Blueprint, request, Response
from flask import request, Response
from flask_jwt_extended import jwt_required, get_jwt_identity
from database.models import Movie, User
from flask_restful import Resource
from mongoengine.errors import (FieldDoesNotExist, NotUniqueError, DoesNotExist,
                                ValidationError, InvalidQueryError)

from resources.errors import (SchemaValidationError, MovieAlreadyExistsError, InternalServerError,
                UpdatingMovieError, DeletingMovieError, MovieNotExistsError)

#movies = Blueprint('movies', __name__)

class MoviesApi(Resource):
    def get(self):
        movies = Movie.objects().to_json()
        return Response(movies, mimetype="application/json", status=200)

    jwt_required()
    def post(self):
        try:
            user_id = get_jwt_identity()
            body = request.get_json()
            #movie = Movie(**body).save()
            user = User.objects.get(id=user_id)
            movie = Movie(**body, added_by=user)
            movie.save()
            user.update(push__movies=movie)
            user.save()
            id = movie.id
            return {'id': str(id)}, 200
        except (FieldDoesNotExist, ValidationError):
            raise SchemaValidationError
        except NotUniqueError:
            raise MovieAlreadyExistsError
        except Exception as e:
            raise InternalServerError
    
class MovieApi(Resource):
    jwt_required()
    def put(self, id):
        try:
            user_id = get_jwt_identity()
            movie = Movie.objects.get(id=id, added_by=user_id)
            body = request.get_json()
            movie.update(**body)
            return '', 200
        except InvalidQueryError:
            raise SchemaValidationError
        except DoesNotExist:
            raise UpdatingMovieError
        except Exception:
            raise InternalServerError

    jwt_required()
    def delete(self, id):
        try:
            user_id = get_jwt_identity()
            movie = Movie.objects.get(id=id, added_by=user_id)
            #Movie.objects.get(id=id).delete()
            movie.delete()
            return '', 200
        except DoesNotExist:
            raise MovieNotExistsError
        except Exception:
            raise InternalServerError

    def get(self, id):
        movies = Movie.objects.get(id=id).to_json()
        return Response(movies, mimetype="application/json", status=200)


''' Exemple with Blueprint
@movies.route('/movies', methods=['GET'])
def get_movies():
    movies = Movie.objects().to_json()
    return Response(movies, mimetype="application/json", status=200)


@movies.route('/movies/<id>', methods=['GET'])
def get_movie(id):
    movies = Movie.objects.get(id=id).to_json()
    return Response(movies, mimetype="application/json", status=200)


@movies.route('/movies', methods=['POST'])
def add_movie():
    body = request.get_json()
    movie = Movie(**body).save()
    id = movie.id
    return {'id': str(id)}, 200


@movies.route('/movies/<id>', methods=['PUT'])
def update_movie(id):
    body = request.get_json()
    Movie.objects.get(id=id).update(**body)
    return '', 200


@movies.route('/movies/<id>', methods=['DELETE'])
def delete_movie(id):
    Movie.objects.get(id=id).delete()
    return '', 200

'''
