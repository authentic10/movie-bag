from .movie import MoviesApi, MovieApi
from .auth import SignUp, Login

def initialize_routes(api):
    #api.add_resource(MoviesApi, '/movies')
    #api.add_resource(MovieApi, '/movies/<id>')
    api.add_resource(MoviesApi, '/api/movies')
    api.add_resource(MovieApi, '/api/movies/<id>')
    api.add_resource(SignUp, '/api/auth/signup')
    api.add_resource(Login, '/api/auth/login')


